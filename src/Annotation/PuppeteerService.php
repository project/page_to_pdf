<?php

namespace Drupal\page_to_pdf\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Puppeteer Service annotation object.
 *
 * @see \Drupal\page_to_pdf\Plugin\PdfGeneratorManager
 * @see plugin_api
 *
 * @Annotation
 */
class PuppeteerService extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
