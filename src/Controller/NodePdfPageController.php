<?php

namespace Drupal\page_to_pdf\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Defines controller for generating PDF file.
 */
class NodePdfPageController extends ControllerBase {

  /**
   * Page to PDF route controller.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $controllerConfigFactory
   *   The config factory.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The current route match.
   */
  public function __construct(
    protected ConfigFactoryInterface $controllerConfigFactory,
    protected RouteMatchInterface $routeMatch,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('current_route_match'),
    );
  }

  /**
   * Render node page to generate PDF.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node.
   *
   * @return string
   *   Return PDF node content.
   */
  public function view(NodeInterface $node) {
    $node_to_view = $node;

    // If route is 'latest', load the latest revision.
    if ($this->routeMatch->getRouteName() === 'page_to_pdf.node_pdf_revision_page_controller') {
      $node_storage = $this->entityTypeManager()->getStorage('node');
      $latest_vid = $node_storage->getLatestRevisionId($node->id());
      $node_to_view = $node_storage->loadRevision($latest_vid);
    }

    $pdf_settings = $this->controllerConfigFactory->get('page_to_pdf.pdf_settings');
    $content_types = $pdf_settings->get('content_types') ?? [];

    // Return page not found error if PDF generating isn't enabled for this
    // content type.
    if (empty($content_types[$node_to_view->getType()])) {
      throw new NotFoundHttpException();
    }

    $view_builder = $this->entityTypeManager()->getViewBuilder('node');
    $build = [
      '#theme' => 'node_pdf_content',
      '#node_view' => $view_builder->view($node_to_view, $content_types[$node_to_view->getType()]['view_mode']),
      '#cache' => [
        'tags' => [
          'node:' . $node_to_view->id(),
        ],
      ],
    ];

    // Add PageJS library.
    if (!empty($content_types[$node_to_view->getType()]['pagejs'])) {
      $build['#attached']['library'][] = 'page_to_pdf/pagedjs';
    }

    return $build;
  }

}
