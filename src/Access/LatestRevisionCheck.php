<?php

namespace Drupal\page_to_pdf\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Access check for the 'Latest version PDF preview' tab.
 *
 * @see \Drupal\content_moderation\Access\LatestRevisionCheck
 */
class LatestRevisionCheck implements AccessInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new LatestRevisionCheck object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Checks that there is a pending revision available.
   *
   * This checker assumes the presence of an '_entity_access' requirement key
   * in the same form as used by EntityAccessCheck.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parametrized route.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   *
   * @see \Drupal\Core\Entity\EntityAccessCheck
   */
  public function access(RouteMatchInterface $route_match, AccountInterface $account) {
    $entity = $route_match->getParameter('node');

    if ($this->hasPendingRevision($entity)) {
      // Check the global permissions first.
      $access_result = AccessResult::allowedIfHasPermissions($account, [
        'view latest version',
        'view any unpublished content',
      ]);

      if (!$access_result->isAllowed()) {
        // Check entity owner access.
        $owner_access = AccessResult::allowedIfHasPermissions($account, [
          'view latest version',
          'view own unpublished content',
        ]);
        $owner_access = $owner_access->andIf((AccessResult::allowedIf($entity instanceof EntityOwnerInterface && ($entity->getOwnerId() == $account->id()))));
        $access_result = $access_result->orIf($owner_access);
      }

      return $access_result->addCacheableDependency($entity);
    }

    return AccessResult::forbidden('No pending revision for moderated entity.')->addCacheableDependency($entity);
  }

  /**
   * Determines if a pending revision exists for the specified entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity which may or may not have a pending revision.
   *
   * @return bool
   *   TRUE if this entity has pending revisions available, FALSE otherwise.
   *
   * @see \Drupal\content_moderation\ModerationInformation::hasPendingRevision()
   */
  protected function hasPendingRevision(ContentEntityInterface $entity): bool {
    $result = FALSE;
    /** @var \Drupal\Core\Entity\RevisionableStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
    $latest_revision_id = $storage->getLatestTranslationAffectedRevisionId($entity->id(), $entity->language()->getId());
    $default_revision_id = $entity->isDefaultRevision() && !$entity->isNewRevision() && ($revision_id = $entity->getRevisionId()) ?
      $revision_id : $this->getDefaultRevisionId($entity->getEntityTypeId(), $entity->id());

    if ($latest_revision_id !== NULL && $latest_revision_id != $default_revision_id) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $latest_revision */
      $latest_revision = $storage->loadRevision($latest_revision_id);
      $result = !$latest_revision->wasDefaultRevision();
    }

    return $result;
  }

  /**
   * Returns the revision ID of the default revision for the specified entity.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param int $entity_id
   *   The entity ID.
   *
   * @return int
   *   The revision ID of the default revision, or NULL if the entity was
   *   not found.
   *
   * @see \Drupal\content_moderation\ModerationInformation::getDefaultRevisionId()
   */
  public function getDefaultRevisionId($entity_type_id, $entity_id) {
    if ($storage = $this->entityTypeManager->getStorage($entity_type_id)) {
      $result = $storage->getQuery()
        ->currentRevision()
        ->condition($this->entityTypeManager->getDefinition($entity_type_id)->getKey('id'), $entity_id)
        // No access check is performed here since this is an API function and
        // should return the same ID regardless of the current user.
        ->accessCheck(FALSE)
        ->execute();

      if ($result) {
        return key($result);
      }
    }

    return NULL;
  }

}
