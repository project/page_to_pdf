<?php

namespace Drupal\page_to_pdf\Plugin\PuppeteerService;

use Drupal\page_to_pdf\Plugin\PuppeteerServiceBase;

/**
 * Provides a browserless.io Puppeteer Service.
 *
 * @PuppeteerService(
 *   id = "browserless",
 *   label = @Translation("Browserless.io"),
 * )
 */
class BrowserlessPuppeteerService extends PuppeteerServiceBase {
}
