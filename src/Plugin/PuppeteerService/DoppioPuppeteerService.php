<?php

namespace Drupal\page_to_pdf\Plugin\PuppeteerService;

use Drupal\page_to_pdf\Plugin\PuppeteerServiceBase;

/**
 * Provides a doppio.sh Puppeteer Service.
 *
 * @PuppeteerService(
 *   id = "doppio",
 *   label = @Translation("Doppio.sh"),
 * )
 */
class DoppioPuppeteerService extends PuppeteerServiceBase {
}
