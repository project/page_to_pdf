<?php

namespace Drupal\page_to_pdf\Plugin\QueueWorker;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\node\NodeInterface;
use Drupal\page_to_pdf\Plugin\PdfGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Generates PDF files for nodes.
 *
 * @QueueWorker(
 *   id = \Drupal\page_to_pdf\Plugin\QueueWorker\PdfGenerator::ID,
 *   title = @Translation("PDF generator"),
 *   cron = {"time" = 60}
 * )
 */
class PdfGenerator extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  public const ID = 'page_to_pdf_generator';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The PDF generator manager.
   *
   * @var \Drupal\page_to_pdf\Plugin\PdfGeneratorManager
   */
  protected $pdfGeneratorManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->configFactory = $container->get('config.factory');
    $instance->pdfGeneratorManager = $container->get('plugin.manager.pdf_generator');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    [$nid, $langcode] = $data;

    $node = $this->entityTypeManager
      ->getStorage('node')
      ->load($nid);
    if (!$node instanceof NodeInterface) {
      return;
    }
    if (!$node->hasTranslation($langcode)) {
      return;
    }

    $content_types = $this->configFactory
      ->get('page_to_pdf.pdf_settings')
      ->get('content_types') ?? [];
    if (!isset($content_types[$node->bundle()])) {
      return;
    }

    $plugin_id = $content_types[$node->bundle()]['plugin'];
    try {
      $pdf_generator = $this->pdfGeneratorManager->createInstance($plugin_id);
      assert($pdf_generator instanceof PdfGeneratorInterface);
    }
    catch (PluginNotFoundException $exception) {
      return;
    }

    $pdf_generator->generateMainPdf($nid, $langcode);
    $pdf_generator->saveFinalPdf($nid, $langcode);
  }

}
