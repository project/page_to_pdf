<?php

namespace Drupal\page_to_pdf\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Puppeteer Service plugins.
 */
abstract class PuppeteerServiceBase extends PluginBase implements PuppeteerServiceInterface {
}
