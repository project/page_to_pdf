<?php

namespace Drupal\page_to_pdf\Plugin\PdfGenerator;

use Drupal\page_to_pdf\Plugin\PdfGeneratorBase;
use Drupal\page_to_pdf\Plugin\PdfGeneratorInterface;
use HeadlessChromium\Browser;
use HeadlessChromium\BrowserFactory;
use HeadlessChromium\Exception\CommunicationException;
use HeadlessChromium\Page;

/**
 * Provides browserless.io PDF generator.
 *
 * @PdfGenerator(
 *   id = "browserless_pdf_generator",
 *   label = @Translation("Browserless.io PDF generator"),
 * )
 */
class BrowserlessPdfGenerator extends PdfGeneratorBase implements PdfGeneratorInterface {

  /**
   * Internal storage for browser object.
   *
   * @var \HeadlessChromium\Browser|null
   */
  private Browser | null $browser = NULL;

  /**
   * Generates PDF from web page with options.
   *
   * @param string $page_url
   *   Web page URL.
   * @param string $output_file_uri
   *   Destination file path.
   * @param array $options
   *   Generation options.
   */
  public function generatePdf(string $page_url, string $output_file_uri, array $options = []): void {
    $context = [
      'page_url' => &$page_url,
      'output_file_uri' => &$output_file_uri,
      'options' => &$options,
    ];

    try {
      $options = array_merge($this->getDefaultPdfOptions(), $options);

      // Connect to browser.
      $browser = $this->getBrowser();
      $context['browser'] = $context;

      // Open new tab.
      $page = $browser->createPage();

      // Set custom User-Agent, so it could be tested with local reverse proxy.
      $page->setUserAgent('page_to_pdf');

      // Allow other modules to modify page.
      $this->moduleHandler->alter('page_to_pdf_page_created', $page, $context);

      /*
       * Rewrite since this will be closed:
       * https://github.com/chrome-php/chrome/issues/41
       */
      // Here will be HTTP status code of request.
      $status_code = NULL;
      // Attach listener to event, when request response received.
      $page->getSession()->on('method:Network.responseReceived',
        function ($params) use (&$status_code, $page_url) {
          // We care just of mainly loaded url, not other assets.
          if ($params['type'] === 'Document'
              && $params['response']['url'] === $page_url) {
            $status_code = $params['response']['status'];
          }
        }
      );

      // Go to page url and wait for networkIdle event
      // (i.e. no network activity for last 500ms).
      $page->navigate($page_url)
        ->waitForNavigation(Page::NETWORK_IDLE);

      $context['status_code'] = $status_code;

      // Let other modules inject custom logic for loaded page.
      $this->moduleHandler->alter('page_to_pdf_page_loaded', $page, $context);

      // Generate a PDF of page.
      $options = array_merge($this->getDefaultPdfOptions(), $options);
      $pdf = $page->pdf($options);

      // Transfer file and save file locally
      // with operation timeout in 5 min (300000 milliseconds).
      $pdf->saveToFile($output_file_uri, $this->pdfTransferTimeout * 1000);
      // Generate a PDF of page.
      $pdf = $page->pdf($options);
      // Transfer file and save file locally
      // with operation timeout in 5 min (300000 milliseconds).
      $pdf->saveToFile($output_file_uri, $this->pdfTransferTimeout * 1000);
    }
    catch (\Exception $e) {
      // Necessarily log error.
      $this->logger
        ->error($this->t("PDF generation failed with exception: '@error_message'.", ["@error_message" => $e->getMessage()]));
    }
    finally {
      try {
        if (isset($page) && $page instanceof Page) {
          $page->close();
        }
      }
      catch (CommunicationException) {
        // Do nothing with this, as we don"t care a lot about that.
      }
    }
  }

  /**
   * Get websocket uri to remote web browser.
   *
   * @return string
   *   Websocket uri string.
   */
  protected function getBrowserSocketUri(): string {
    // Use browser socket from settings if exists.
    if ($websocket = $this->settings->get('page_to_pdf_browser')) {
      return $websocket;
    }

    if ($api_key = $this->settings->get('page_to_pdf_api_key')) {
      return 'wss://chrome.browserless.io/webdriver?token=' . $api_key;
    }
    return '';
  }

  /**
   * Get browser connection.
   *
   * @return \HeadlessChromium\Browser
   *   Browser instance.
   *
   * @throws \HeadlessChromium\Exception\BrowserConnectionFailed
   */
  public function getBrowser(): Browser {
    if (!$this->browser instanceof Browser) {
      $this->browser = BrowserFactory::connectToBrowser(
        $this->getBrowserSocketUri()
      );
    }
    return $this->browser;
  }

  /**
   * Get default options for PDF generation.
   *
   * @return array
   *   List of options.
   *
   * @see \HeadlessChromium\Page::pdf
   */
  protected function getDefaultPdfOptions(): array {
    return [
      'paperWidth' => 8.3,
      'paperHeight' => 11.7,
      'landscape' => FALSE,
      'scale' => 1.0,
      'preferCSSPageSize' => TRUE,
      'marginTop' => 0,
      'marginBottom' => 0,
      'marginLeft' => 0,
      'marginRight' => 0,
      'displayHeaderFooter' => FALSE,
      'printBackground' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function generateMainPdf(int $node_id, string $language_id): void {
    $node = $this->loadNode($node_id);
    if ($node->language()->getId() !== $language_id
        && $node->hasTranslation($language_id)) {
      $node = $node->getTranslation($language_id);
    }

    $this->generatePdf(
      $this->getNodePdfPage($node),
      $this->getTemporaryFilePath($node)
    );
  }

}
