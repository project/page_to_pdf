<?php

namespace Drupal\page_to_pdf\Plugin\PdfGenerator;

use Drupal\Component\Serialization\Json;
use Drupal\Core\File\Exception\FileException;
use Drupal\page_to_pdf\Plugin\PdfGeneratorBase;
use Drupal\page_to_pdf\Plugin\PdfGeneratorInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Provides doppio.sh PDF generator.
 *
 * @PdfGenerator(
 *   id = "doppio_pdf_generator",
 *   label = @Translation("Doppio.sh PDF generator"),
 * )
 */
class DoppioPdfGenerator extends PdfGeneratorBase implements PdfGeneratorInterface {

  /**
   * Get default options for PDF generation.
   *
   * @return array
   *   List of options.
   *
   * @see https://doc.doppio.sh/guide/render-methods/params-pdf.html
   */
  protected function getDefaultPdfOptions(): array {
    return [
      'width' => 8.3,
      'height' => 11.7,
      'landscape' => FALSE,
      'scale' => 1.0,
      'preferCSSPageSize' => TRUE,
      'displayHeaderFooter' => FALSE,
      'printBackground' => TRUE,
    ];
  }

  /**
   * Generates PDF from web page with options.
   *
   * @param string $page_url
   *   Web page URL.
   * @param string $output_file_uri
   *   Destination file path.
   * @param array $options
   *   Generation options.
   */
  public function generatePdf(string $page_url, string $output_file_uri, array $options = []): void {
    try {
      $options = array_merge($this->getDefaultPdfOptions(), $options);

      $page_host = parse_url($page_url, PHP_URL_SCHEME) . '://' . parse_url($page_url, PHP_URL_HOST);
      $ngrok_host = $this->settings->get('ngrok_url');

      if ($ngrok_host) {
        // Use the ngrok URL when using DDEV Local with remote browser.
        $page_url = str_replace($page_host, $ngrok_host, $page_url);
      }

      if (
        getenv('IS_DDEV_PROJECT') == 'true'
        && str_contains($page_url, getenv('DDEV_TLD'))
        && !$ngrok_host
      ) {
        // Local developer help message.
        $message = $this->t('Please see project README.md for how to set up ngrok for testing PDF generation locally in DDEV.');
        $this->messenger->addWarning($message);
      }

      $api_key = $this->settings->get('page_to_pdf_api_key');
      $doppio_endpoint = "https://api.doppio.sh/v1/render/pdf/direct";

      $headers = [
        'Authorization' => 'Bearer ' . $api_key,
        'Content-Type' => 'application/json',
      ];

      $body = [
        "page" => [
          "pdf" => $options,
          "setUserAgent" => "page_to_pdf",
          "goto" => [
            "url" => $page_url,
            "options" => [
              /* cspell:disable-next-line */
              "waitUntil" => ["load", "networkidle0"],
            ],
          ],
        ],
      ];

      $response = $this->httpClient->request('POST', $doppio_endpoint, [
        'headers' => $headers,
        'body' => Json::encode($body),
      ]);
      $this->saveDoppioPdfToFile($response, $output_file_uri);
    }
    catch (\Exception $e) {
      // Necessarily log error.
      $this->logger
        ->error($this->t("PDF generation failed with exception: '@error_message'.", ["@error_message" => $e->getMessage()]));
    }
    catch (GuzzleException $e) {
      $this->logger
        ->error($this->t("PDF generation http request failed with exception: '@error_message'.", ["@error_message" => $e->getMessage()]));
    }
  }

  /**
   * Save Doppio PDF data to the given file.
   *
   * @param mixed $response
   *   The response from Doppio.
   * @param string $path
   *   Destination file path.
   */
  protected function saveDoppioPdfToFile(mixed $response, string $path): void {
    // PDF content.
    $pdf_data = $response->getBody()->getContents();

    // Create directory.
    $directory = $this->fileSystem->dirname($path);
    if (!$this->fileSystem->prepareDirectory($directory)) {
      if (!$this->fileSystem->mkdir($directory)) {
        throw new FileException(\sprintf('Could not create the directory %s.', $directory));
      }
    }

    // Save the file.
    if (file_exists($path) && !is_writable($path)) {
      throw new FileException(\sprintf('The file %s is not writable.', $path));
    }

    if (!$this->fileSystem->saveData($pdf_data, $path)) {
      throw new FileException(\sprintf('The file %s could not be saved.', $path));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function generateMainPdf(int $node_id, string $language_id): void {
    $node = $this->loadNode($node_id);
    if ($node->language()->getId() !== $language_id
        && $node->hasTranslation($language_id)) {
      $node = $node->getTranslation($language_id);
    }

    $this->generatePdf(
      $this->getNodePdfPage($node),
      $this->getTemporaryFilePath($node)
    );
  }

}
