<?php

namespace Drupal\page_to_pdf\Plugin\Filter;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Make the CK Editor 4 page break output more usable by the front-end team.
 *
 * @Filter(
 *   id = "filter_pagebreaks",
 *   title = @Translation("Page breaks filter"),
 *   description = @Translation("Improve the page break output markup adding a CSS class of 'u-page-break'."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class PageBreakFilter extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a PageBreakFilter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected ModuleHandlerInterface $moduleHandler,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    if ($this->moduleHandler->moduleExists('pagebreak')) {
      $text = str_replace('<div style="page-break-after:', '<div class="u-page-break" style="page-break-after:', $text);
    }
    return new FilterProcessResult($text);
  }

}
