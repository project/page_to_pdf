<?php

namespace Drupal\page_to_pdf\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\file\FileInterface;
use Drupal\user\UserInterface;

/**
 * Defines an interface for PDF provider plugins.
 */
interface PdfGeneratorInterface extends PluginInspectionInterface {

  /**
   * Generate PDF file based on node page.
   *
   * @param int $node_id
   *   The node id.
   * @param string $language_id
   *   The node language id.
   */
  public function generateMainPdf(int $node_id, string $language_id): void;

  /**
   * Return final PDF file.
   *
   * @param int $node_id
   *   The node id.
   * @param string $language_id
   *   The node language id.
   *
   * @return \Drupal\file\FileInterface|null
   *   PDF file object.
   */
  public function getFinalPdf(int $node_id, string $language_id): ?FileInterface;

  /**
   * Save temporary PDF file into node.
   *
   * @param int $node_id
   *   The node id.
   * @param string $language_id
   *   The node language id.
   * @param \Drupal\user\UserInterface|null $user
   *   The user object is used as media author.
   */
  public function saveFinalPdf(int $node_id, string $language_id, ?UserInterface $user = NULL): void;

  /**
   * Generates PDF from web page with options.
   *
   * @param string $page_url
   *   Web page absolute url.
   * @param string $output_file_uri
   *   Destination file path.
   * @param array $options
   *   Generation options.
   */
  public function generatePdf(string $page_url, string $output_file_uri, array $options = []): void;

}
