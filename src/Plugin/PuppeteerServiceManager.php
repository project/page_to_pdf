<?php

namespace Drupal\page_to_pdf\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the PDF generator Puppeteer Service plugin manager.
 */
class PuppeteerServiceManager extends DefaultPluginManager {

  /**
   * Constructs a new PdfGeneratorManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/PuppeteerService',
      $namespaces,
      $module_handler,
      'Drupal\page_to_pdf\Plugin\PuppeteerServiceInterface',
      'Drupal\page_to_pdf\Annotation\PuppeteerService',
    );
    $this->alterInfo('page_to_pdf_puppeteer_service_plugin_info');
    $this->setCacheBackend($cache_backend, 'page_to_pdf_puppeteer_service_plugin_plugins');
  }

}
