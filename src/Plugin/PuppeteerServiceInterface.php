<?php

namespace Drupal\page_to_pdf\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Puppeteer Service plugins.
 *
 * @todo define required methods.
 */
interface PuppeteerServiceInterface extends PluginInspectionInterface {
}
