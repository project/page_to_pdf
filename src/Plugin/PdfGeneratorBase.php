<?php

namespace Drupal\page_to_pdf\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Render\PlainTextOutput;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Drupal\node\NodeInterface;
use Drupal\page_to_pdf\Form\NodeTypeFormPdfPartialForm;
use Drupal\user\UserInterface;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for PDF generator plugins.
 */
abstract class PdfGeneratorBase extends PluginBase implements ContainerFactoryPluginInterface {

  use LoggerChannelTrait;
  use StringTranslationTrait;

  /**
   * Logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Timeout seconds to wait for PDF document transferred locally.
   *
   * @var int
   */
  public int $pdfTransferTimeout = 120;

  /**
   * Storage for the redirect repository.
   *
   * @var mixed|null
   */
  protected mixed $redirectRepository = NULL;

  /**
   * Constructs a BlockRegion plugin instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   * @param \Drupal\Core\Utility\Token $token
   *   The token replacement instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param \Drupal\Core\Site\Settings $settings
   *   The site settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \GuzzleHttp\Client $httpClient
   *   The default http client.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   * @param mixed|null $redirectRepository
   *   The placeholder to store the redirect repository if exists.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected ModuleHandlerInterface $moduleHandler,
    protected FileSystemInterface $fileSystem,
    protected Token $token,
    protected ConfigFactoryInterface $configFactory,
    protected Settings $settings,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected Client $httpClient,
    protected Messenger $messenger,
    mixed $redirectRepository = NULL,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $this->getLogger('page_to_pdf');
    if ($redirectRepository) {
      $this->redirectRepository = $redirectRepository;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('file_system'),
      $container->get('token'),
      $container->get('config.factory'),
      $container->get('settings'),
      $container->get('entity_type.manager'),
      $container->get('http_client'),
      $container->get('messenger'),
      $container->has('redirect.repository') ? $container->get('redirect.repository') : NULL,
    );
  }

  /**
   * Static method to run own plugin step method.
   *
   * If run any methods from this plugin type in the batch directly, you will
   * get the error about dependencies which cannot be serialized. This static
   * method helps to solver this problem. The two first arguments should be
   * plugin id and method name.
   *
   * @param mixed ...$args
   *   Method arguments.
   *
   * @todo Find another way to run method directly from plugin object instead of
   * generating service object for each step.
   */
  public static function runPluginStep(...$args): void {
    [$plugin_id, $method] = $args;
    $context = &$args[array_key_last($args)];

    // This is a static method, dependency injection cannot be used.
    // phpcs:ignore
    $manager = \Drupal::service('plugin.manager.pdf_generator');
    $pdf_provider = $manager->createInstance($plugin_id);
    try {
      call_user_func_array([$pdf_provider, $method], array_slice($args, 2));
    }
    catch (\Throwable $e) {
      $context['message'] = $e->getMessage();
      $context['finished'] = 0;
      $current_set = &_batch_current_set();
      // Empty remaining items to end batch, as we can't continue with errors.
      $current_set['count'] = 0;
    }
  }

  /**
   * Get batch operations.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node.
   *
   * @return array
   *   Batch operations array.
   */
  public function batchSteps(NodeInterface $node): array {
    $operations = [
      [
        [__CLASS__, 'runPluginStep'],
        [
          $this->pluginId,
          'generateMainPdf',
          $node->id(),
          $node->language()->getId(),
        ],
      ],
      [
        [__CLASS__, 'runPluginStep'],
        [
          $this->pluginId,
          'saveFinalPdf',
          $node->id(),
          $node->language()->getId(),
          NULL,
        ],
      ],
      [
        [__CLASS__, 'runPluginStep'],
        [
          $this->pluginId,
          'generateBatchPdfLink',
          $node->id(),
          $node->language()->getId(),
        ],
      ],
    ];

    // Allowing other modules to add batch operations.
    $this->moduleHandler->alter('page_to_pdf_node_operations', $node, $operations);

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function saveFinalPdf(int $node_id, string $language_id, ?UserInterface $user = NULL): void {
    $node = $this->loadNode($node_id);
    if ($node->language()->getId() !== $language_id
        && $node->hasTranslation($language_id)) {
      $node = $node->getTranslation($language_id);
    }

    // Move temporary file to permanent location.
    $temp_location = $this->getTemporaryFilePath($node);
    $permanent_location = $this->getFilePath($node);
    $this->fileSystem->prepareDirectory($permanent_location, FileSystemInterface::CREATE_DIRECTORY);

    // Create new file and media entities and save into node field.
    $field_name = $this->getSettings($node->getType())['field_save'];
    if (
      !$node->get($field_name)->isEmpty()
      && $node->get($field_name)->entity instanceof MediaInterface
    ) {
      $media = $node->get($field_name)->entity;
      $source_field = $media->getSource()->getConfiguration()['source_field'];

      // Remove old file from media object.
      if (
        !$media->get($source_field)->isEmpty()
        && $media->get($source_field)->entity instanceof FileInterface
      ) {
        $old_file = $media->get($source_field)->entity;
        $old_path = substr($old_file->createFileUrl(), 1);
        $old_file->delete();
      }

      $file = $this->createFile($temp_location, $permanent_location . '/' . $this->getFileName($node));
      $file->save();

      // @todo Create an event type and move it there.
      if (isset($old_path) && $this->moduleHandler->moduleExists('redirect')) {
        $new_file_path = substr($file->createFileUrl(), 1);

        // Set a redirect from the old file to the new one if the redirect
        // module exists.
        if ($this->moduleHandler->moduleExists('redirect')) {
          // This module may not exist, so avoid dependency injection of it.
          if (is_object($this->redirectRepository) && method_exists($this->redirectRepository, 'findBySourcePath')) {
            foreach ($this->redirectRepository->findBySourcePath($old_path) as $redirect) {
              $redirect->setSource($new_file_path);
              $redirect->save();
            }
          }
        }
      }

      $media->set($source_field, ['target_id' => $file->id()]);
      $media->save();
    }
    else {
      $file = $this->createFile($temp_location, $permanent_location . '/' . $this->getFileName($node));
      $file->save();

      $media = $this->createMedia($node, $file);
      if (isset($user)) {
        $media->set('uid', $user->id());
      }
      $media->save();
      $node->set($field_name, $media);
      $node->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFinalPdf(int $node_id, string $language_id): ?FileInterface {
    $node = $this->loadNode($node_id);
    if ($node->language()->getId() !== $language_id
        && $node->hasTranslation($language_id)) {
      $node = $node->getTranslation($language_id);
    }

    $field_name = $this->getSettings($node->getType())['field_save'];
    if (!$node->get($field_name)->isEmpty()) {
      $media = $node->get($field_name)->entity;
      if ($media && $media instanceof MediaInterface) {
        $source_field = $media->getSource()->getConfiguration()['source_field'];
        if (!$media->get($source_field)->isEmpty()) {
          return $media->get($source_field)->entity;
        }
      }
    }

    return NULL;
  }

  /**
   * Add link in batch context.
   */
  public function generateBatchPdfLink(int $node_id, string $language_id, &$context): void {
    if ($file = $this->getFinalPdf($node_id, $language_id)) {
      $context['results']['file_link'] = $file->createFileUrl();
    }
  }

  /**
   * Load node by id.
   *
   * @param int $node_id
   *   Node id.
   *
   * @return \Drupal\node\NodeInterface|null
   *   Node entity object.
   */
  public function loadNode(int $node_id): ?NodeInterface {
    return $this->entityTypeManager
      ->getStorage('node')
      ->load($node_id);
  }

  /**
   * Generate temporary file path base on node id.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object for generating unique temporary path.
   *
   * @return string
   *   Temporary file path.
   */
  protected function getTemporaryFilePath(NodeInterface $node): string {
    return $this->fileSystem->getTempDirectory() . "/temp-file-{$node->id()}.pdf";
  }

  /**
   * Generate file name base on node title.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object for generating filename.
   *
   * @return string
   *   Generated filename.
   */
  protected function getFileName(NodeInterface $node): string {
    $file_name = $this->token->replace('[node:title] ([node:nid])', ['node' => $node]);
    $file_name = str_replace([' ', '/'], '-', $file_name);
    $file_name = strtolower($file_name);
    return "{$file_name}.pdf";
  }

  /**
   * Generate file path base on field settings.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object for getting field.
   *
   * @return string
   *   Generated file path.
   */
  protected function getFilePath(NodeInterface $node): string {
    $media = $this->createMedia($node);

    $source_field = $media->getSource()->getConfiguration()['source_field'];
    $settings = $media->get($source_field)->getSettings();

    $destination = trim($settings['file_directory'], '/');

    // Replace tokens. As the tokens might contain HTML we convert it to plain
    // text.
    $destination = PlainTextOutput::renderFromHtml($this->token->replace($destination));
    return $settings['uri_scheme'] . '://' . $destination;
  }

  /**
   * Create media object.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node object to get settings.
   * @param \Drupal\file\FileInterface|null $file
   *   PDF file object to save into new media.
   *
   * @return \Drupal\media\MediaInterface
   *   Media object.
   */
  protected function createMedia(NodeInterface $node, ?FileInterface $file = NULL): MediaInterface {
    $field_name = $this->getSettings($node->getType())['field_save'];
    $handler_settings = $node->get($field_name)->getSetting('handler_settings');
    $media_pdf_bundles = NodeTypeFormPdfPartialForm::getAvailableMediaBundles();

    // Get first PDF media bundle.
    foreach ($handler_settings['target_bundles'] as $bundle) {
      if (in_array($bundle, $media_pdf_bundles)) {
        break;
      }
      $bundle = NULL;
    }
    if (!isset($bundle)) {
      throw new \Exception("The media field doesn't have any media PDF type.");
    }

    $media = Media::create(['bundle' => $bundle]);
    if (isset($file)) {
      $source_field = $media->getSource()->getConfiguration()['source_field'];
      $media->set($source_field, ['target_id' => $file->id()]);
    }

    return $media;
  }

  /**
   * Move temporary file to permanent folder and create file object.
   *
   * @param string $temp_location
   *   Temporary location.
   * @param string $destination
   *   Destination path.
   *
   * @return \Drupal\file\FileInterface
   *   Created file object.
   */
  protected function createFile(string $temp_location, string $destination): FileInterface {
    $uri = $this->fileSystem->move($temp_location, $destination);
    $file = File::create([
      'filename' => basename($destination),
      'status' => FileInterface::STATUS_PERMANENT,
      'uri' => $uri,
    ]);
    return $file;
  }

  /**
   * Get PDF settings for node type.
   *
   * @param string $node_type
   *   The node type.
   */
  protected function getSettings(string $node_type): array {
    $content_types = $this->configFactory->get('page_to_pdf.pdf_settings')
      ->get('content_types') ?? [];
    return $content_types[$node_type] ?? [];
  }

  /**
   * Generate link to node PDF view mode for generating PDF.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node.
   *
   * @return string
   *   The URL for the node.
   */
  protected function getNodePdfPage(NodeInterface $node): string {
    return Url::fromRoute('page_to_pdf.node_pdf_page_controller', [
      'node' => $node->id(),
    ])
      ->setOption('language', $node->language())
      ->setAbsolute()
      ->toString();
  }

}
