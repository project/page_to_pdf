<?php

namespace Drupal\page_to_pdf_test\Plugin\PuppeteerService;

use Drupal\page_to_pdf\Plugin\PuppeteerServiceBase;

/**
 * Provides a Test Puppeteer Service.
 *
 * @PuppeteerService(
 *   id = "test_pdf",
 *   label = @Translation("Test PDF"),
 * )
 */
class TestPuppeteerService extends PuppeteerServiceBase {
}
