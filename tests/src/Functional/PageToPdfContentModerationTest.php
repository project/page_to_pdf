<?php

namespace Drupal\Tests\page_to_pdf\Functional;

use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;
use Drupal\user\Entity\Role;

/**
 * Contains Page to PDF content moderation functional tests.
 *
 * @group page_to_pdf
 */
class PageToPdfContentModerationTest extends PageToPdfTestBase {

  use ContentModerationTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'page_to_pdf',
    'page_to_pdf_test',
    'node',
    'user',
    'system',
    'media',
    'file',
    'field_ui',
    'workflows',
    'content_moderation',
    'block',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    drupal_flush_all_caches();

    $workflow = $this->createEditorialWorkflow();
    $this->addEntityTypeAndBundleToWorkflow($workflow, 'node', 'article');
    $workflow->save();

    $this->drupalPlaceBlock('local_tasks_block');
    drupal_flush_all_caches();

    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'administer content types',
      'access content overview',
      'access content',
      'view any unpublished content',
      'administer nodes',
      'administer node fields',
      'bypass node access',
      'administer media types',
      'administer workflows',
      'administer permissions',
      'use editorial transition archive',
      'use editorial transition create_new_draft',
      'use editorial transition publish',
      'view latest version',
    ]);

    $content_type_id = 'article';
    $role_ids = $this->adminUser->getRoles(TRUE);
    /** @var \Drupal\user\RoleInterface $role */
    $role_id = reset($role_ids);
    $role = Role::load($role_id);
    $role->grantPermission(sprintf('create %s content', $content_type_id));
    $role->grantPermission(sprintf('edit any %s content', $content_type_id));
    $role->grantPermission(sprintf('delete any %s content', $content_type_id));
    $role->save();

    drupal_flush_all_caches();
    $this->drupalLogin($this->adminUser);
    drupal_flush_all_caches();
  }

  /**
   * Tests content moderation links.
   */
  public function testContentModeration(): void {
    // Create a node and a forward revision.
    $this->drupalGet('node/add/article');
    $this->submitForm([
      'title[0][value]' => 'Moderated Article',
      'moderation_state[0][state]' => 'published',
    ], 'Save');

    // Get the created node by title.
    $entity_type_manager = $this->container->get('entity_type.manager');
    $nodes = $entity_type_manager->getStorage('node')->loadByProperties([
      'type' => 'article',
      'title' => 'Moderated Article',
    ]);
    $node = reset($nodes);

    // Create a forward revision.
    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->submitForm([
      'title[0][value]' => 'Moderated Article (Updated)',
      'moderation_state[0][state]' => 'draft',
    ], 'Save');
    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->submitForm([
      'title[0][value]' => 'Moderated Article (Updated 2)',
      'moderation_state[0][state]' => 'draft',
    ], 'Save');

    // Verify the forward revision was created.
    $this->drupalGet('node/' . $node->id() . '/revisions');
    $this->assertSession()->pageTextContains('Moderated Article (Updated 2)');

    // Test that the latest version PDF preview link exists.
    $this->drupalGet('node/' . $node->id() . '/edit');
    $session = $this->assertSession();
    $session->linkExists('PDF preview');

    // Verify the PDF preview link points to the correct URL.
    $pdf_preview_link = $this->getSession()->getPage()->findLink('PDF preview')->getAttribute('href');
    $pdf_preview_link = str_replace('/web', '', $pdf_preview_link);
    $expected_pdf_preview_url = '/node/' . $node->id() . '/pdf';
    $this->assertStringContainsString($pdf_preview_link, $expected_pdf_preview_url);

    // Get the published version and expect to see a link to the latest version.
    $this->drupalGet('node/' . $node->id());
    $session = $this->assertSession();
    $session->linkExists('Latest version PDF preview');

    // Double-check access to the latest version.
    $this->drupalGet('node/' . $node->id() . '/pdf/latest');
    $this->assertSession()->statusCodeEquals(200);
  }

}
