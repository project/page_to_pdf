<?php

namespace Drupal\Tests\page_to_pdf\Functional;

use Drupal\Tests\language\Traits\LanguageTestTrait;
use Drupal\user\Entity\Role;

/**
 * Contains Page to PDF language functional tests.
 *
 * @group page_to_pdf
 */
class PageToPdfLanguageTest extends PageToPdfTestBase {

  use LanguageTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'page_to_pdf',
    'page_to_pdf_test',
    'node',
    'user',
    'system',
    'media',
    'file',
    'field_ui',
    'language',
    'locale',
    'content_translation',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createLanguageFromLangcode('nl');
    $this->enableBundleTranslation('node', 'article', 'en');
    $this->setFieldTranslatable('node', 'article', 'field_pdf', TRUE);
    drupal_flush_all_caches();

    $role_ids = $this->adminUser->getRoles(TRUE);
    /** @var \Drupal\user\RoleInterface $role */
    $role_id = reset($role_ids);
    $role = Role::load($role_id);
    $role->grantPermission('create content translations');
    $role->grantPermission('update content translations');
    $role->grantPermission('translate any entity');
    $role->grantPermission('administer content translation');
    $role->grantPermission('administer languages');
    $role->save();
    $this->drupalLogin($this->adminUser);
    drupal_flush_all_caches();
  }

  /**
   * Tests creating a PDF translation with default settings.
   */
  public function testGenerateTranslatedPdf(): void {

    // Create a node and expect to generate immediately.
    $this->drupalGet('node/add/article');
    $this->submitForm([
      'title[0][value]' => 'Test 1 EN',
    ], 'Save and generate PDF');

    // Get the created node by title in case the tests are run in parallel.
    $entity_type_manager = $this->container->get('entity_type.manager');
    $nodes = $entity_type_manager->getStorage('node')->loadByProperties([
      'type' => 'article',
      'title' => 'Test 1 EN',
    ]);
    /** @var \Drupal\node\NodeInterface $node */
    $node = reset($nodes);

    // Check original.
    $this->getSession()->getPage()->findLink('test-1-en-(' . $node->id() . ').pdf')->click();
    $this->assertSession()->pageTextContains('Page URL is');
    $this->assertSession()->pageTextContains('/node/' . $node->id() . '/pdf');

    // Make sure article is translatable.
    $this->drupalGet('admin/config/regional/content-language');
    $this->submitForm([
      'settings[node][article][translatable]' => TRUE,
      'settings[node][article][fields][field_pdf]' => TRUE,
      'settings[node][article][fields][title]' => TRUE,
    ], 'Save configuration');
    $this->drupalGet('admin/config/regional/language');

    // Translate the article.
    $this->drupalGet('nl/node/' . $node->id() . '/translations/add/en/nl');
    $this->submitForm([
      'title[0][value]' => 'Test 1 NL',
    ], 'Save and generate PDF');
    $this->getSession()->getPage()->findLink('test-1-nl-(' . $node->id() . ').pdf')->click();
    $this->assertSession()->pageTextContains('Page URL is');
    $this->assertSession()->pageTextContains('/node/' . $node->id() . '/pdf');
  }

}
