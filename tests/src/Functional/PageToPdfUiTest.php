<?php

namespace Drupal\Tests\page_to_pdf\Functional;

/**
 * Contains Page to PDF UI functional tests.
 *
 * @group page_to_pdf
 */
class PageToPdfUiTest extends PageToPdfTestBase {

  /**
   * Tests save and generate button exists.
   */
  public function testSaveAndGenerateButton(): void {

    // Create a node, expect to see the generate button.
    $this->drupalGet('node/add/article');
    $this->assertSession()->buttonExists('Save and generate PDF');
    $this->submitForm([
      'title[0][value]' => 'Test 1',
    ], 'Save');
  }

  /**
   * Tests creating a PDF with default settings.
   */
  public function testGeneratePdf(): void {

    // Create a node and expect to generate immediately.
    $this->drupalGet('node/add/article');
    $this->submitForm([
      'title[0][value]' => 'Test 1',
    ], 'Save and generate PDF');

    // Get the created node by title in case the tests are run in parallel.
    $entity_type_manager = $this->container->get('entity_type.manager');
    $nodes = $entity_type_manager->getStorage('node')->loadByProperties([
      'type' => 'article',
      'title' => 'Test 1',
    ]);
    $node = reset($nodes);

    // Check there is a PDF preview and it correctly renders the teaser view
    // mode.
    $this->drupalGet('node/' . $node->id() . '/pdf');
    $session = $this->assertSession();
    $session->pageTextContains($node->label());
    $session->pageTextContains('Read more');

    // Test generating the PDF.
    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->submitForm([], 'Save and generate PDF');
    $this->assertSession()->pageTextContains('The PDF file was successfully generated.');
    $this->getSession()->getPage()->findLink('test-1-(' . $node->id() . ').pdf')->click();
    $this->assertSession()->pageTextContains('Page URL is');
    $this->assertSession()->pageTextContains('/node/' . $node->id() . '/pdf');
  }

  /**
   * Tests queue worker.
   */
  public function testQueueWorker(): void {

    // Save settings with queue worker.
    $this->drupalGet('admin/structure/types/manage/article');
    $this->submitForm([
      'method' => 'automatic',
    ], 'Save');

    // Create a node, expect to see the generate button.
    $this->drupalGet('node/add/article');
    $this->assertSession()->buttonNotExists('Save and generate PDF');
    $this->submitForm([
      'title[0][value]' => 'Test 2',
    ], 'Save');

    // Get the created node by title in case the tests are run in parallel.
    $entity_type_manager = $this->container->get('entity_type.manager');
    $nodes = $entity_type_manager->getStorage('node')->loadByProperties([
      'type' => 'article',
      'title' => 'Test 2',
    ]);
    $node = reset($nodes);

    // Verify there is no PDF yet.
    $this->drupalGet('node/' . $node->id());
    $has_link = $this->getSession()->getPage()->hasLink('test-' . $node->id() . '-(1).pdf');
    $this->assertFalse($has_link);

    /** @var \Drupal\Core\Queue\QueueInterface $queue */
    $queue = $this->container->get('queue')->get('page_to_pdf_generator', TRUE);
    /** @var \Drupal\page_to_pdf\Plugin\QueueWorker\PdfGenerator $queue_worker */
    $queue_worker = $this->container->get('plugin.manager.queue_worker')->createInstance('page_to_pdf_generator');
    $item = $queue->claimItem();
    $this->assertIsNotBool($item);
    $this->assertIsNumeric($item->data[0]);
    $this->assertSame($node->id(), $item->data[0]);
    $this->assertSame('en', $item->data[1]);

    $this->drupalGet('admin/structure/types/manage/article/fields');
    $queue_worker->processItem($item->data);

    // Check that a PDF was generated.
    $this->drupalGet('node/' . $node->id());
    $this->getSession()->getPage()->findLink('test-2-(' . $node->id() . ').pdf')->click();
    $this->assertSession()->pageTextContains('Page URL is');
    $this->assertSession()->pageTextContains('/node/' . $node->id() . '/pdf');
  }

}
