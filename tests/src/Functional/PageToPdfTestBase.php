<?php

namespace Drupal\Tests\page_to_pdf\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Contains Page to PDF UI functional tests.
 *
 * @group page_to_pdf
 */
abstract class PageToPdfTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'page_to_pdf',
    'page_to_pdf_test',
    'node',
    'user',
    'system',
    'media',
    'file',
    'field_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission to bypass access content.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create Basic page and Article node types.
    if ($this->profile != 'standard') {
      $this->drupalCreateContentType([
        'type' => 'article',
        'name' => 'Article',
      ]);
    }

    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'administer content types',
      'access content overview',
      'administer nodes',
      'administer node fields',
      'bypass node access',
      'administer media types',
    ]);

    $this->setUpPageToPdfUi();
  }

  /**
   * Set up initial Page to PDF settings.
   */
  protected function setUpPageToPdfUi(): void {
    $this->drupalLogin($this->adminUser);

    // Expect media reference to not exist yet.
    $this->drupalGet('admin/structure/types/manage/article');
    $this->submitForm([
      'pdf_enable' => TRUE,
    ], 'Save');
    $this->assertSession()->pageTextContains('"Media Reference field" is required');

    // Create media type.
    $this->drupalGet('admin/structure/media/add');
    $this->submitForm([
      'label' => 'PDF',
      'id' => 'pdf',
      'source' => 'file',
    ], 'Save');
    $this->getSession()->getPage()->pressButton('Save');

    // Create a media file field on the article.
    $this->drupalGet('admin/structure/types/manage/article/fields/add-field');
    $this->submitForm([
      'new_storage_type' => 'field_ui:entity_reference:media',
    ], 'Continue');
    $this->submitForm([
      'label' => 'PDF',
      'field_name' => 'pdf',
    ], 'Continue');
    $this->submitForm([], 'Save settings');
    $this->submitForm([
      'settings[handler_settings][target_bundles][pdf]' => 'pdf',
    ], 'Save settings');

    // Save settings.
    $this->drupalGet('admin/structure/types/manage/article');
    $this->submitForm([
      'pdf_enable' => TRUE,
      'pdf_field_save' => 'field_pdf',
      'pdf_view_modes' => 'teaser',
      'pdf_plugin' => 'test_pdf',
    ], 'Save');

    // So the new field is found on the node bundle.
    drupal_flush_all_caches();
  }

}
