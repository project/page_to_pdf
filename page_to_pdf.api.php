<?php

/**
 * @file
 * Documentation related to page_to_pdf.
 */

use HeadlessChromium\Page;

/**
 * Fired right after browser tab was opened.
 *
 * This option works with the Browserless generator only.
 */
function hook_page_to_pdf_page_created_alter(Page $page, array &$context) {
  // Set custom User-Agent, so it could be tested with local reverse proxy.
  $page->setUserAgent('page_to_pdf');

  // Or override base url.
  $context['page_url'] = 'https://example.com';
}

/**
 * Fired when page was loaded.
 *
 * This option works with the Browserless generator only.
 */
function hook_page_to_pdf_page_loaded_alter(Page $page, array &$context) {
  // Check if page loaded successfully.
  if ($context['status_code'] === 301) {
    \Drupal::logger('page_to_pdf')->info('Message code: ' . $context['status_code']);
  }
}

/**
 * Adding some new operation to PDF generation batch.
 *
 * This option only works with the 'manual' trigger using the "Save & Generate
 * PDF" button. To implement extra operations during the automated option that
 * runs via Queue worker, react to node insert and update events like
 * hook_node_insert() and hook_node_update().
 */
function hook_page_to_pdf_node_operations_alter($node, &$operations) {
  $operations[] = [
    'operation_callback',
    [$node],
  ];
}
